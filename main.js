const sketchScreen = document.querySelector("#sketch-container");
let gridDimension = 80;

createGrid(gridDimension);

sketchScreen.style.backgroundColor = "white";



function createGrid(n)
{
  sketchScreen.style.height = `76vmin`;
  sketchScreen.style.width = `76vmin`;
  sketchScreen.style.border = `solid rgb(221, 21, 21) 12vmin`;
  sketchScreen.style.borderRadius = "35px";
  
  for(let y = n - 1; y >= 0; y--)
  {
    const row = document.createElement('div');
    for(let x = 0; x < n; x++)
    {
      const square = document.createElement('div');
      row.appendChild(square);
      square.style.height = '100%';
      square.style.width = `${100/n}%`;
      square.setAttribute('data-x',`${x}`);
      square.setAttribute('data-y', `${y}`);
      square.style.backgroundColor = 'rgb(255, 255, 255)';
      square.addEventListener("mouseover", darken);
      
    }
  
    row.style.display = 'flex';
    sketchScreen.appendChild(row);
    row.style.height = `${100/n}%`;    
  }
}

 const RGB_Linear_Shade=(p,c)=>{
  var i=parseInt,r=Math.round,[a,b,c,d]=c.split(","),P=p<0,t=P?0:255*p,P=P?1+p:1-p;
  return"rgb"+(d?"a(":"(")+r(i(a[3]=="a"?a.slice(5):a.slice(4))*P+t)+","+r(i(b)*P+t)+","+r(i(c)*P+t)+(d?","+d:")");
}

 function darken()
  {
    this.style.backgroundColor = RGB_Linear_Shade(-0.80, this.style.backgroundColor);
    let xValue = parseInt(this.getAttribute('data-x'));
    let yValue = parseInt(this.getAttribute('data-y'));
   
    // if(xValue > 0) shade(xValue - 1, yValue);
    // if(yValue > 0) shade(xValue, yValue - 1);
    // if(xValue < (gridDimension - 1)) shade((xValue + 1), yValue);
    // if(yValue < (gridDimension - 1)) shade(xValue, (yValue + 1));
  }

  function shade(x, y)
  {
    const currentSquare = document.querySelector(`[data-x = "${x}"][data-y = "${y}"]`);

    currentSquare.style.backgroundColor = RGB_Linear_Shade(-0.1, currentSquare.style.backgroundColor);
    
  }